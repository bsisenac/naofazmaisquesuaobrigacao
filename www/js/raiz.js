// Objeto global responsável por carregar informações como URL do Portal, url de login.
var goAplicativo = new AplicativoClass();
function AplicativoClass(){ 
    
    this.urlLogin = "users/login_app";
    this.nomeApp = "webapp";
    this.versao = "0.0.4";
    
    // Função que retorna a url
    this.url = function( url ){
        url = url || "";
        
        return "http://swshopping.duvasconcelos.com.br/" + url;
//        return "http://paie-hom.nicdev.com.br/" + url;    

    }
    
    // Função que retorna a url sem HTTP
    this.urlNoHTTP = function( url ){
        url = url || "";
//        return "paie-hom.nicdev.com.br/" + url;    
        return "swshopping.duvasconcelos.com.br/" + url;    
    }
    
    // Serviço de notificações por push
    this.serviceNotificacoes = function( ){
//        
//        console.log( window.plugins.OneSignal );
//        
//        document.addEventListener('deviceready', function () {
//          // Enable to debug issues.
//          // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
//
//          var notificationOpenedCallback = function(jsonData) {
//            msg('notificationOpenedCallback: ' + JSON.stringify(jsonData) );
//          };
////
//          window.plugins.OneSignal
//            .startInit("a4a49f5b-1804-4c2a-bab9-958604edbfcf", "454041250724")
//            .handleNotificationOpened(notificationOpenedCallback)
//            .endInit();
//          }, false);
    };
    
    this.findAll = function( model, funcao ){
        
//        return '{ "success": true, "failed": false, "warning": false, "message": [{"User":{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"},"Grupo":{"id":"1","name":"administrador","created":"2016-11-07 19:13:19","modified":"2016-11-18 19:14:40","User":[{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"},{"id":"2","grupo_id":"1","username":"beardtech@beardtech.com.br","nome":"Beard Tech","sobrenome":"LTDA","password":"$2a$10$O8mPHo8Yl.9HYPZitiHdY.m2Oh7unOfz5tvyOOsyJeBGwoLv43DY6","created":"2016-11-09 14:36:13","modified":"2016-11-09 14:36:13","fullname":"2 - Beard Tech"},{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}]},"Log":[],"Notificacao":[{"id":"1","texto":"teste","user_id":"1","User":{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"}},{"id":"3","texto":"a","user_id":"1","User":{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"}},{"id":"4","texto":"Oi","user_id":"1","User":{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"}}],"Nota":[{"id":"3","nota":"9","disciplina_id":"1","user_id":"1","created":"2016-11-24 22:04:26","modified":"2016-11-24 22:04:26","Disciplina":{"id":"1","nome":"Hist\u00f3ria","codigo":"H1"},"User":{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"}}]},{"User":{"id":"2","grupo_id":"1","username":"beardtech@beardtech.com.br","nome":"Beard Tech","sobrenome":"LTDA","password":"$2a$10$O8mPHo8Yl.9HYPZitiHdY.m2Oh7unOfz5tvyOOsyJeBGwoLv43DY6","created":"2016-11-09 14:36:13","modified":"2016-11-09 14:36:13","fullname":"2 - Beard Tech"},"Grupo":{"id":"1","name":"administrador","created":"2016-11-07 19:13:19","modified":"2016-11-18 19:14:40","User":[{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"},{"id":"2","grupo_id":"1","username":"beardtech@beardtech.com.br","nome":"Beard Tech","sobrenome":"LTDA","password":"$2a$10$O8mPHo8Yl.9HYPZitiHdY.m2Oh7unOfz5tvyOOsyJeBGwoLv43DY6","created":"2016-11-09 14:36:13","modified":"2016-11-09 14:36:13","fullname":"2 - Beard Tech"},{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}]},"Log":[],"Notificacao":[],"Nota":[]},{"User":{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"},"Grupo":{"id":"1","name":"administrador","created":"2016-11-07 19:13:19","modified":"2016-11-18 19:14:40","User":[{"id":"1","grupo_id":"1","username":"erodrigues@nic.br","nome":"Eduardo","sobrenome":"Vasconcelos","password":"$2a$10$CVcLdshpgyflWiO1Ju8wtuIWPPB5tA\/tOn3sTu41.M1Jd6bsW0gUu","created":"2016-11-07 19:13:53","modified":"2016-11-07 19:13:53","fullname":"1 - Eduardo"},{"id":"2","grupo_id":"1","username":"beardtech@beardtech.com.br","nome":"Beard Tech","sobrenome":"LTDA","password":"$2a$10$O8mPHo8Yl.9HYPZitiHdY.m2Oh7unOfz5tvyOOsyJeBGwoLv43DY6","created":"2016-11-09 14:36:13","modified":"2016-11-09 14:36:13","fullname":"2 - Beard Tech"},{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}]},"Log":[],"Notificacao":[{"id":"2","texto":"affe maria","user_id":"3","User":{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}}],"Nota":[{"id":"1","nota":"10","disciplina_id":"1","user_id":"3","created":"2016-11-23 17:31:57","modified":"2016-11-23 17:31:57","Disciplina":{"id":"1","nome":"Hist\u00f3ria","codigo":"H1"},"User":{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}},{"id":"2","nota":"5","disciplina_id":"1","user_id":"3","created":"2016-11-24 21:30:56","modified":"2016-11-24 21:30:56","Disciplina":{"id":"1","nome":"Hist\u00f3ria","codigo":"H1"},"User":{"id":"3","grupo_id":"1","username":"ranselmo@nic.br","nome":"Rafael","sobrenome":"Anselmo","password":"$2a$10$xADnj7b77udjckPo\/xvOoebCiznWete\/N497gdWZuCPZrzqr8uN8i","created":"2016-11-16 15:41:13","modified":"2016-11-16 15:41:13","fullname":"3 - Rafael"}}]}]}';
        var retorno;
        
        if( typeof funcao == "undefined" ){
            
            var teste = { "data": getProfile() } ;
            
            jQuery.ajax({
                type: "POST",
                url: goAplicativo.url( model + "/getAll_APP" ),
                data: teste,
                success: function (result) {
                    retorno = result;
                
                },
                async: false
            });
            return retorno;
            
        }else{
            $.get( url, funcao );
        };
        
        return true
    };
};

// Objeto global responsável pelo uso do banco de dados
var goDb = new DbClass();
function DbClass(){
    
    /********************************************* PROPRIEDADES **************************************************************/
    this.db = null;
    this.request = null;
    this.nomeBanco = "appWeb"; 
    this.versaoBanco = 1;
    
    /********************************************* FUNÇÕES *******************************************************************/
    
    /* Método construtor */
    this.init = function(){
        this.db = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        this.verificar();
    };
    
    // Responsável pela abertura do banco
    this.abrir = function( nomeTabela ){
        var req =  this.db.open( this.nomeBanco, this.versaoBanco );
        var db;
        
        req.onsuccess = function (e) {
          db = this.result;
        };
        
        req.onerror = function (e) {
          exception( "Erro de Sistema",  "Não foi possível abrir/criar o banco de dados: "  + e.target.errorCode);
        };
        
        this.db.result = db;
        
    };

    // Responsável pela abertura do banco
    this.criarBanco = function( ){
        
        var req =  this.db.open( this.nomeBanco, this.versaoBanco );
        var db;
        req.onsuccess = function (e) {
          db = this.result;
        };
        
        req.onerror = function (e) {
          exception( "Erro de Sistema",  "Não foi possível abrir/criar o banco de dados: "  + e.target.errorCode);
        };

        req.onupgradeneeded = function (e) {
            
          var store = e.currentTarget.result.createObjectStore( "Users", { keyPath: 'id', autoIncrement: false } );
          store.createIndex( 'nome', 'nome', { unique: false });
          store.createIndex( 'username', 'username', { unique: true } );
          store.createIndex( 'password', 'password', { unique: false } );
            
        };
        
        this.db.result = db;
        
    };
    
    // Responsável por verificar se o dispositivo tem suporte a indexDb 
    this.verificar = function(){
        if( ! this.db ){
            exception( "Erro de sistema", "Dispositivo sem suporte a acesso a dados locais." )
        }
        return true;
    };
    
    
    this.requestOnSuccess = function(e){
      this.resultado = this.request.result;  
    };
    
    this.requestOnError = function(e){
      exception( "Erro de Sistema",  "Não foi possível abrir/criar o banco de dados: "  + event.target.errorCode );
    };
    
    // Método construtor da classe
    this.init();
}


/************************************************************ Funções auxiliares ************************************************************/

// Função com as informações iniciais
function init(){
    
    btnToggleCollapse();
    btnTogglePageSideBar();
    btnSideBarVoltar();
//    goAplicativo.serviceNotificacoes();   
    datepickerCarregar();
    btnSair();
    salvarConta();
    
}

// Função responsável por abrir as páginas
function abrir_pagina( pagina ){
    // Colocar as opções adicionais caso haja necessidades
    // Como por exemplo permissões de acesso, etc.
    activate_page( pagina );
}

// Seta um valor a "sessão"
function setSession( nome, conteudo ){     
    window.localStorage.setItem( nome, conteudo );
}

// Retorna um valor da sessão
function getSession( nome ){
    return window.localStorage.getItem( nome );
}

// Exibe uma  mensagem de erro no topo da página ou através de um elemento passado como parâmetro
function msg( mensagem, tipo, strElemento ){
    
    if( typeof strElemento == "undefined" )
        elemento = $( ".upage" ).not( ".hidden" );
    else
        elemento = $( strElemento );
    
    tipo = tipo || "warning";
    
    var html = '<div id="msg" class="alert alert-' + tipo + '">' + mensagem + '</div>';
    limparMsg();
    elemento.prepend( html );   
}

// Limpar a mensagem enviada como alerta
function limparMsg(){
    $( "#msg" ).remove();
}

// Função responsável pelas mensagens de erro
function exception( name, mensagem ){
    throw { 
            name:        name, 
            level:       "Show Stopper", 
            message:     mensagem,
            htmlMessage: "Erro: Entre em contato com a equipe de desenvolvimento: <a href=\"mailto:suporte@beardtech.com\">suporte beardtech</a>.",
            toString:    function(){return this.name + ": " + this.message + " - versão: " + goAplicativo.versao;}
        }; 
}

