/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
    
     init();
    
        /* button  #btn-entrar */
    $(document).on("click", "#btn-entrar", function(evt)
    {
         abrir_login();
    });
    
        
        /* button  #btnFilhos */
    $(document).on("click", "#btnFilhos", function(evt)
    {
         /*global activate_page */
         abrir_pagina( "#filhos" );
         btnFilhos(this);
         return false;
    });
    
        /* button  #loginEsqueciSenha */
    $(document).on("click", "#loginEsqueciSenha", function(evt)
    {
        /* your code goes here */ 
        abrir_pagina( "#get_camara" );
        return false;
    });
    
        /* button  #loginEsqueciSenha */
    $(document).on("click", "#btnConta", function(evt)
    {
        /* your code goes here */ 
        abrir_pagina( "#conta" );
        return false;
    });
    
    }
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
