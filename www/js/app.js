function abrir_login(){
    
    var data = { "data": { User: { username: $( "#username" ).val(), password: $( "#password" ).val()  }}};
    var post = $.post( goAplicativo.url( goAplicativo.urlLogin ), data, loginRealizado );
    post.error( erro_login );
    return false;
}

function loginRealizado( success ){
    
    var retorno = JSON.parse( success );
    if( retorno.success ){
    
        setSession( "User", success );
        preencher_dados_pessoais();
        limparMsg();
        abrir_pagina( "#mainpage" ); 

    }else{
        msg( retorno.message, "danger" );
    }
}

function preencher_dados_pessoais(){
    
    $( "#nome-perfil" ).text( getProfile().User.nome + " " + getProfile().User.sobrenome );
    $( "#username-perfil" ).text( getProfile().User.username );
    
    if( getProfile().User.url_img )
        $( "#imgPerfil" ).attr( "src", getProfile().User.url_img );
    
}

function erro_login( fail ){
    msg( "Não foi possível acessar o servidor!", "danger" );
}

function btnToggleCollapse(){
    
    $( '[data-toggle="collapse"]' ).unbind( "click" );
    $('[data-toggle="collapse"]').click( function( e ){
        e.preventDefault();
        var id = $(this).data("target");
        $(id).toggle();
    });
}

function btnTogglePageSideBar(){
    $('[data-toggle="page-pos-sidebar"]').click( function( e ){
        
        e.preventDefault();
        $('[data-toggle="page-pos-sidebar"]').removeClass("data-toogle-select");
        $(this).addClass("data-toogle-select");

        var id = $(this).data("target");
        $( ".page-pos-sidebar.collapse" ).hide();
        $(id).toggle();
    });
}

function btnSideBarVoltar(e){
    
    $('.btnSideBarVoltar').click( function(){
        abrir_pagina("#mainpage");
    });
    
    $('.btnCancelar').click( function(){
        abrir_pagina("#mainpage");
        preencher_dados_pessoais();
    });
    
}

function datepickerCarregar( datepicker_id ){
    
    
    $( datepicker_id ).datepicker( ).datepicker( 'disable' );
    $( datepicker_id ).datepick( 
        { multiSelect: 999, 
          dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
          navigationAsDateFormat: true,
          nextText: 'Próximo', 
          prevText: 'Anterior',
          todayText: 'Hoje',
          monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
         showMonthBeforeYear: false,
        }
    );
    
    $( datepicker_id ).datepick( 'setDate', '11/17/2016,11/18/2016,11/21/2016,11/3/2016'.split( "," ) );
}
function btnTituloMenu( e ){
    $( ".titulo-menu" ).click( function() {
        $( ".titulo-menu" )
    })
}

function getProfile(){
    
    return JSON.parse( getSession( "User" ) ).message;
    
}

function btnFilhos( button ){
    
    var listFilhos = JSON.parse( goAplicativo.findAll( "users" ) );
    mostrarNoticias( listFilhos );
    mostrarFaltas( listFilhos );
    mostrarNotas( listFilhos );

    function mostrarNoticias( listFilhos ){
        
        var raiz = $( '#filhos-noticias' ).find( ".mostrar-filhos" );
        raiz.html( "" );
        $.each( listFilhos.message, function( index, obj ){
            
            var user = obj.User;
            var nome = user.nome + " " + user.sobrenome;
            var html = '<div class="panel panel-info panel-list"><div class="panel-heading"><button class="btn-transparent btn-no-border btn-head-panel-list" data-toggle="collapse" data-target="#panel-body-noticia-' + index + '">' + nome + '</button></div><div class="panel-body collapse" id="panel-body-noticia-' + index + '"><div class="fundo">';
            
            if( obj.Notificacao.length == 0 ){
                html += '<div class="alert alert-warning">Sem notícias lançadas."</div>';
            }else{
                $.each( obj.Notificacao, function( index, obj ){
                    html += '<p class="dados-list-filho"><span class="nome-tipo"> ' + obj.texto + '</span></p>';
                });
            };
            html += '</div></div></div>';
                   

            raiz.append( html );
            
        } );
        
        btnToggleCollapse();

    };
    
    function mostrarFaltas( listFilhos ){

        var raiz = $( '#filhos-faltas' ).find( ".mostrar-filhos" );
        raiz.html( "" );
        $.each( listFilhos.message, function( index, obj ){
            
            var user = obj.User;
            var nome = user.nome + " " + user.sobrenome;
            var datepicker_id = 'datepicker-' + index;
            var html = '<div class="panel panel-info panel-list"><div class="panel-heading"><button class="btn-transparent btn-no-border btn-head-panel-list" data-toggle="collapse" data-target="#panel-body-faltas-' + index + '">' + nome + '</button></div><div class="panel-body collapse" id="panel-body-faltas-' + index + '">';
            
            if( obj.Notificacao.length == 0 ){
                html += '<div class="alert alert-warning">Sem faltas lançadas."</div>';
            }else{
                html += '<div class="datepicker-alunos" id="' + datepicker_id + '"></div>';
            };
            
            html += '</div></div>';

            raiz.append( html );
            datepickerCarregar( "#" + datepicker_id );
            
        } );
        
        btnToggleCollapse();

    };
    
    function mostrarNotas( listFilhos ){
        
        var raiz = $( '#filhos-notas' ).find( ".mostrar-filhos" );
        raiz.html( "" );
        $.each( listFilhos.message, function( index, obj ){
            
            var user = obj.User;
            var nome = user.nome + " " + user.sobrenome;
            var html = '<div class="panel panel-info panel-list"><div class="panel-heading"><button class="btn-transparent btn-no-border btn-head-panel-list" data-toggle="collapse" data-target="#panel-body-notas-' + index + '">' + nome + '</button></div><div class="panel-body collapse" id="panel-body-notas-' + index + '">';
            
            if( obj.Nota.length == 0 ){
                html += '<p class="dados-list-filho"><div class="alert alert-warning">Sem notas lançadas.</div</p>';
            }else{
                $.each( obj.Nota, function( index, obj ){
                    html += '<p class="dados-list-filho"><span class="nome-tipo"> Avaliação de ' + obj.Disciplina.nome + '</span>';
                    html += '<span class="valor"> - ' + obj.nota + '</span></p>'
                });
            }
            
            html += '</div></div>';
                   

            raiz.append( html );
            
        } );
        
        btnToggleCollapse();

    };
    
}

function btnSair(){
    
    $( "#btnSair" ).click( function(e) {
        abrir_pagina("#login");
    });
}

function salvarConta(){
    
    $( "#salvarConta" ).click( function(){

        var form = new FormData();
        form.append("file", $("#imgPerfil").attr("src")  );
        form.append("user_id", getProfile().User.id );

    
        var settings = {
          "async": false,
          "crossDomain": true,
          "url": goAplicativo.url( "users/upload_app" ),
          "method": "POST",
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }

        $.ajax(settings).done(function (response) {
          console.log(response);
          abrir_pagina("#mainpage")
        }); 
    });
}